// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.impl;

import 'package:shelf/shelf.dart';
import 'package:path/path.dart' as p;

import 'api.dart';

import 'router_builder.dart';
import 'router_builder_impl.dart';
import 'default_adapters.dart';
import 'core.dart';
import 'default_route.dart';

final _url = p.url;

class ShelfRouteRouterBuilder<B extends ShelfRouteRouterBuilder>
    extends DefaultRouterBuilderImpl<B> implements Router<B> {
  final Handler fallbackHandler;
  DefaultRouter _router;

  ShelfRouteRouterBuilder(Function fallbackHandler, String name, path,
      RouterAdapter routerAdapter, routeable, Middleware middleware)
      : this._(
            fallbackHandler, name, path, routerAdapter, routeable, middleware);

  ShelfRouteRouterBuilder.def(path,
      {String name,
      Function fallbackHandler,
      RouterAdapter routeAdapter,
      routeable,
      Middleware middleware})
      : this._(
            fallbackHandler, name, path, routeAdapter, routeable, middleware);

  ShelfRouteRouterBuilder.create(
      {String name,
      Function fallbackHandler,
      HandlerAdapter handlerAdapter,
      RouteableAdapter routeableAdapter,
      PathAdapter pathAdapter,
      Middleware middleware,
      path,
      routeable})
      : this._(
            fallbackHandler,
            name,
            path,
            new DefaultRouterBuilderAdapter(
                handlerAdapter, pathAdapter, routeableAdapter),
            routeable,
            middleware);

  ShelfRouteRouterBuilder._(Function fallbackHandler, String name, path,
      RouterAdapter routerAdapter, routeable, Middleware middleware)
      : this._2(
            fallbackHandler != null ? fallbackHandler : send404,
            name,
            path != null ? path : '/',
            routerAdapter != null
                ? routerAdapter
                : const DefaultRouterBuilderAdapter.def(),
            routeable,
            middleware);

  ShelfRouteRouterBuilder._2(Function fallbackHandler, String name, path,
      DefaultRouterAdapter routerAdapter, routeable, Middleware middleware)
      : this.fallbackHandler = routerAdapter.handlerAdapter(fallbackHandler),
        super(path,
            name: name,
            routeAdapter: routerAdapter,
            routeable: routeable,
            middleware: middleware);

  Iterable<PathInfo> get fullPaths => router.flattenedPaths;

  DefaultRouter get router {
    if (_router == null) {
      // TODO: avoid this as can be confusing to users who may add more routes later
      _router = build();
    }
    return _router;
  }

  Handler get handler => router.createHandler(fallbackHandler);

  ShelfRouteRouterBuilder<B> createChild(String name, path, routeable,
          RouterAdapter routerAdapter, Middleware middleware) =>
      new ShelfRouteRouterBuilder(
          null, name, path, routerAdapter, routeable, middleware);
}

Response send404(Request req) {
  return new Response.notFound("Not Found");
}
