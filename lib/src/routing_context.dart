// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.routing.context;

import 'package:shelf/shelf.dart';
import 'route.dart';
import 'package:option/option.dart';
import 'default_route.dart';

/// Provides contextual information about the routing process. In particular
/// it provides access to the [currentRoute] the request is passing through and
/// to the path through the router hierarchy that the request
/// has so far traveled (via [routingBreadCrumbs])
abstract class RoutingContext {
  /// The path the request has travelled through to router hierarchy.
  /// Note if you are using more exotic routers then you will need to access
  /// via [rawRoutingOrder] instead
  Iterable<DefaultRoute> get routingOrder;

  /// The current [DefaultRoute] being processed.
  /// This is a [DefaultSimpleRoute] if the request
  /// has reached it's destination handling route or a [DefaultRouter] if the
  /// request is still in transit
  DefaultRoute get currentRoute;

  DefaultRouter get containingRouter;

  /// This is intended for advanced uses only. It provides access to the actual
  /// [Route] objects which are less convenient for typical use cases but
  /// useful in cases where more exotic routers are used
  Iterable<Route> get rawRoutingOrder;

//  Option<Route> findRouteByIdPath(String routeIdPath);
//
//  Uri getRouteUri(Route route, {bool absolute});
//
//  Response redirectTo(Route route);
}

/// Retrieves the current [RoutingContext] for the given [Request] or [None]
Option<RoutingContext> getRoutingContext(Request request) =>
    new Option(request.context[_routingContextParamName]);

RoutingContext appendToRoutingBreadCrumbs(Map context, Route route) {
  final routingContext = _getOrCreateRoutingContext(context);
  routingContext.rawRoutingOrder.add(route);
  return routingContext;
}

class _RoutingContextImpl implements RoutingContext {
  @override
  final List<Route> rawRoutingOrder = [];

  @override
  Iterable<DefaultRoute> get routingOrder => rawRoutingOrder.map((r) {
        if (r is! DefaultRoute) {
          throw new ArgumentError(
              'You are using exotic routes so must access via rawRoutingOrder instead');
        }
        return r;
      });

  @override
  DefaultRouter get containingRouter {
    if (routingOrder.length < 2) {
      throw new RangeError('There is no containing router');
    }
    return routingOrder.elementAt(routingOrder.length - 2);
  }

  @override
  DefaultRoute get currentRoute {
    if (routingOrder.length < 1) {
      throw new RangeError('There is no current route');
    }
    return routingOrder.last;
  }
}

const String _routingContextParamName = 'shelf_route.routing.context';

_RoutingContextImpl _getOrCreateRoutingContext(Map context) => context
    .putIfAbsent(_routingContextParamName, () => new _RoutingContextImpl());
