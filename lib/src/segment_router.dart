// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.segment;

import 'route.dart';
import 'package:shelf/shelf.dart';
import 'core.dart';
import 'dart:async';

/// handles a single segment of a route
abstract class SegmentRouter {
  bool canHandle(Request request);

  Future<Response> handle(Request request, Handler handler, Route route);

  SegmentRouter join(SegmentRouter child);

  PathInfo get pathInfo;
}
