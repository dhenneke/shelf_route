// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.builder.impl;

import 'package:shelf/shelf.dart';
import 'package:path/path.dart' as p;
import 'router_builder.dart';
import 'default_adapters.dart';
import 'core.dart';
import 'preconditions.dart';
import 'package:option/option.dart';
import 'default_route.dart';

final _url = p.url;

class SpecBasedRouterBuilderImpl<B extends DefaultRouterBuilder>
    implements SpecBasedRouterBuilder<B> {
  final String name;
  final path;
  final List<RouteSpec> routes = [];
  final RouterAdapter routeAdapter;
  final Option routeable;
  final Middleware middleware;

  SpecBasedRouterBuilderImpl(this.path,
      {this.name,
      this.middleware,
      RouterAdapter routeAdapter: const DefaultRouterAdapter.def(),
      routeable})
      : this.routeAdapter = routeAdapter != null
            ? routeAdapter
            : const DefaultRouterAdapter.def(),
        this.routeable = new Option(routeable) {
    ensure(this.routeAdapter, isNotNull);
  }

  @override
  void addRoute(RouteSpec route) {
    routes.add(route);
  }

  @override
  void addSimpleRoute(SimpleRouteSpec route) {
    addRoute(route);
  }

  @override
  void addRouter(RouterSpec router) {
    addRoute(router);
  }

  @override
  DefaultRouter build() => routeAdapter.adapt(this);
}

class DefaultRouterBuilderImpl<B extends DefaultRouterBuilder>
    extends SpecBasedRouterBuilderImpl<B> implements DefaultRouterBuilder<B> {
  DefaultRouterBuilderImpl(path,
      {String name,
      RouterAdapter routeAdapter: const DefaultRouterAdapter.def(),
      routeable,
      Middleware middleware})
      : super(path,
            name: name,
            routeAdapter: routeAdapter,
            routeable: routeable,
            middleware: middleware);

  void add(dynamic path, List<String> methods, Function handler,
      {String name,
      bool exactMatch: true,
      Middleware middleware,
      HandlerAdapter handlerAdapter,
      PathAdapter pathAdapter}) {
    addSimpleRoute(new DefaultSimpleRouteSpec(
        name,
        path,
        methods,
        exactMatch,
        handler,
        new DefaultSimpleRouteAdapter(handlerAdapter, pathAdapter),
        middleware));
  }

  void addAll(routeable,
      {String name,
      dynamic path,
      RouteableAdapter routeableAdapter,
      PathAdapter pathAdapter,
      HandlerAdapter handlerAdapter,
      Middleware middleware}) {
    // TODO: This is now identical to child but doesn't return the child.
    // Also routeable is mandatory here and path isn't where as in child it is
    // the other way around.
    // Do they still serve a useful purpose or should we deprecate child and
    // return from addAll???
    child(path,
        name: name,
        handlerAdapter: handlerAdapter,
        pathAdapter: pathAdapter,
        middleware: middleware,
        routeableAdapter: routeableAdapter,
        routeable: routeable);
  }

  B child(dynamic path,
      {String name,
      RouteableAdapter routeableAdapter,
      routeable,
      PathAdapter pathAdapter,
      HandlerAdapter handlerAdapter,
      Middleware middleware}) {
    final childRouter = createChild(
        name,
        path,
        routeable,
        new DefaultRouterBuilderAdapter(
            handlerAdapter, pathAdapter, routeableAdapter),
        middleware);

    addRouter(childRouter);
    return childRouter;
  }

  /// Subclasses should override to create an instance of the same type
  DefaultRouterBuilderImpl<B> createChild(String name, path, routeable,
          RouterAdapter routerAdapter, Middleware middleware) =>
      new DefaultRouterBuilderImpl(path,
          name: name,
          routeAdapter: routerAdapter,
          routeable: routeable,
          middleware: middleware);

  @override
  void get(dynamic path, Function handler,
      {String name, Middleware middleware, HandlerAdapter handlerAdapter}) {
    add(path, ['GET'], handler,
        name: name, middleware: middleware, handlerAdapter: handlerAdapter);
  }

  @override
  void post(dynamic path, Function handler,
      {String name, Middleware middleware, HandlerAdapter handlerAdapter}) {
    add(path, ['POST'], handler,
        name: name, middleware: middleware, handlerAdapter: handlerAdapter);
  }

  @override
  void put(dynamic path, Function handler,
      {String name, Middleware middleware, HandlerAdapter handlerAdapter}) {
    add(path, ['PUT'], handler,
        name: name, middleware: middleware, handlerAdapter: handlerAdapter);
  }

  @override
  void delete(dynamic path, Function handler,
      {String name, Middleware middleware, HandlerAdapter handlerAdapter}) {
    add(path, ['DELETE'], handler,
        name: name, middleware: middleware, handlerAdapter: handlerAdapter);
  }
}

class DefaultRouteSpec<R extends DefaultRoute> implements RouteSpec<R> {
  final String name;
  final path;
  final RouteAdapter<R> routeAdapter;
  final Middleware middleware;

  DefaultRouteSpec(this.name, this.path, this.routeAdapter, this.middleware);
}

class DefaultSimpleRouteSpec extends DefaultRouteSpec<DefaultSimpleRoute>
    implements SimpleRouteSpec {
  final Iterable<String> methods;
  final bool exactMatch;
  final Function handler;

  DefaultSimpleRouteSpec(String name, path, this.methods, this.exactMatch,
      this.handler, SimpleRouteAdapter routeAdapter, Middleware middleware)
      : super(name, path, routeAdapter, middleware);
}

class DefaultRouterSpec extends DefaultRouteSpec<DefaultRouter>
    implements RouterSpec {
  final Iterable<RouteSpec> routes;

  DefaultRouterSpec(String name, path, this.routes, RouterAdapter routeAdapter,
      Middleware middleware)
      : super(name, path, routeAdapter, middleware);
}

class RouteableRouterSpec extends DefaultRouterSpec {
  final Option<RouteableFunction> routeable;

  RouteableRouterSpec(String name, path, Iterable<RouteSpec> routes,
      RouterAdapter routeAdapter, this.routeable, Middleware middleware)
      : super(name, path, routes, routeAdapter, middleware);
}
