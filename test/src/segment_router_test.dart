library shelf_route.router.fundamentals.test;

import 'dart:async';

import 'package:mockito/mockito.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_route/src/default_segment_router.dart';
import 'package:shelf_route/src/route.dart';
import 'package:test/test.dart';
import 'package:uri/uri.dart';

import 'helpers.dart';

class MockRoute extends Mock implements SimpleRoute {}

main() {
  DefaultSegmentRouter createRouter(
          String path, Iterable<String> methods, bool exactMatch) =>
      new DefaultSegmentRouter(
          path != null ? new UriParser(new UriTemplate(path)) : null,
          methods,
          exactMatch);

  group('construction', () {
    test('null method allowed', () {
      expect(() => createRouter('', null, false), isNotNull);
    });
  });

  group('handle for null method', () {
    DefaultSegmentRouter router;

    routeGroup(String desc, String method,
        {String requestPath,
        String expectedPathAfter,
        String expectedHandlerPathAfter}) {
      _routeGroup(desc, () => router, method, requestPath, expectedPathAfter,
          expectedHandlerPathAfter);
    }

    setUp(() {
      router = createRouter('foo', null, false);
    });

    test("returns false when path doesn't match", () {
      expect(
          router.canHandle(
              new Request('GET', Uri.parse('http://example.com/barney'))),
          false);
    });

    test("returns false when path doesn't match", () {
      expect(
          router.canHandle(
              new Request('GET', Uri.parse('http://example.com/food'))),
          false);
    });

    routeGroup("when path matches exactly", 'GET',
        requestPath: 'foo',
        expectedPathAfter: '',
        expectedHandlerPathAfter: '/foo');

    routeGroup("when path matches exactly and method PUT", 'PUT',
        requestPath: 'foo',
        expectedPathAfter: '',
        expectedHandlerPathAfter: '/foo');

    routeGroup("when path matches sub path", 'GET',
        requestPath: 'foo/bar',
        expectedPathAfter: 'bar',
        expectedHandlerPathAfter: '/foo/');
  });

  group('handle for multi segment path', () {
    DefaultSegmentRouter router;

    routeGroup(String desc, String method,
        {String requestPath,
        String expectedPathAfter,
        String expectedHandlerPathAfter}) {
      _routeGroup(desc, () => router, method, requestPath, expectedPathAfter,
          expectedHandlerPathAfter);
    }

    setUp(() {
      router = createRouter('foo/bar', null, false);
    });

    test("returns false when path doesn't match", () {
      expect(
          router.canHandle(
              new Request('GET', Uri.parse('http://example.com/foo'))),
          false);
    });

    routeGroup("when path matches exactly", 'GET',
        requestPath: 'foo/bar',
        expectedPathAfter: '',
        expectedHandlerPathAfter: '/foo/bar');

    routeGroup("when path matches sub path", 'GET',
        requestPath: 'foo/bar/blah',
        expectedPathAfter: 'blah',
        expectedHandlerPathAfter: '/foo/bar/');
  });

  group('handle for null path', () {
    DefaultSegmentRouter router;

    routeGroup(String desc, String method,
        {String requestPath,
        String expectedPathAfter,
        String expectedHandlerPathAfter}) {
      _routeGroup(desc, () => router, method, requestPath, expectedPathAfter,
          expectedHandlerPathAfter);
    }

    setUp(() {
      router = createRouter(null, null, false);
    });

    test("returns true as path always matches", () {
      expect(
          router.canHandle(
              new Request('GET', Uri.parse('http://example.com/foo'))),
          true);
    });

    routeGroup("when any path", 'GET',
        requestPath: 'foo/bar',
        expectedPathAfter: 'foo/bar',
        expectedHandlerPathAfter: '/');
  });

  group('handle for some methods', () {
    DefaultSegmentRouter router;

    routeGroup(String desc, String method,
        {String requestPath,
        String expectedPathAfter,
        String expectedHandlerPathAfter}) {
      _routeGroup(desc, () => router, method, requestPath, expectedPathAfter,
          expectedHandlerPathAfter);
    }

    setUp(() {
      router = createRouter('foo', ['GET', 'DELETE'], false);
    });

    test("returns false when method doesn't match", () {
      expect(
          router.canHandle(
              new Request('PUT', Uri.parse('http://example.com/barney'))),
          false);
    });

    routeGroup("when method matches", 'GET',
        requestPath: 'foo',
        expectedPathAfter: '',
        expectedHandlerPathAfter: '/foo');

    routeGroup("when method matches", 'DELETE',
        requestPath: 'foo',
        expectedPathAfter: '',
        expectedHandlerPathAfter: '/foo');
  });

  group('join', () {
    DefaultSegmentRouter resultRouter12;
    DefaultSegmentRouter resultRouter21;
    setUp(() {
      final router1 = createRouter('foo', ['GET', 'DELETE'], false);
      final router2 = createRouter('bar', ['PUT', 'DELETE'], true);
      resultRouter12 = router1.join(router2);
      resultRouter21 = router2.join(router1);
    });
    test('inherits child exactMatch', () {
      expect(resultRouter12.exactMatch, true);
      expect(resultRouter21.exactMatch, false);
    });
    test('takes intersetction of methods', () {
      expect(resultRouter12.methods, unorderedEquals(['DELETE']));
      expect(resultRouter21.methods, unorderedEquals(['DELETE']));
    });
    test('joins paths together', () {
      expect(resultRouter12.path.toString(), 'foo/bar');
      expect(resultRouter21.path.toString(), 'bar/foo');
    });
  });
}

_routeGroup(
    String desc,
    DefaultSegmentRouter router(),
    String method,
    String requestPath,
    String expectedPathAfter,
    String expectedHandlerPathAfter) {
  group(desc, () {
    new RouteTests.create(router, method, requestPath, expectedPathAfter,
            expectedHandlerPathAfter)
        .addTests();
  });
}

typedef DefaultSegmentRouter DefaultSegmentRouterFactory();
typedef Request RequestFactory();

class RouteTests {
  final RequestFactory requestFactory;
  Request get request => requestFactory();
//  final Request expectedRequestAfter;
  final DefaultSegmentRouterFactory routerFactory;
  DefaultSegmentRouter get router => routerFactory();
  final String expectedPathAfter;
  final String expectedHandlerPathAfter;

  RouteTests(this.routerFactory, this.requestFactory, this.expectedPathAfter,
      this.expectedHandlerPathAfter);

  RouteTests.create(DefaultSegmentRouterFactory router, String method,
      String path, String expectedPathAfter, String expectedHandlerPathAfter)
      : this(
            router,
            () => new Request(method, Uri.parse('http://example.com/$path')),
            expectedPathAfter,
            expectedHandlerPathAfter);

  void addTests() {
    TestHandler handler;

    setUp(() {
      handler = new TestHandler('foo');
    });

    Future<Request> handle() async {
      await router.handle(request, handler, new MockRoute());
      return handler.calledRequest;
    }

    test("returns non null request", () async {
      expect((await handle()), isNotNull);
    });

    test("returns a request with expected url", () async {
      expect((await handle()).url, equals(Uri.parse(expectedPathAfter)));
    });

    test("returns a request with expected method", () async {
      expect((await handle()).method, equals(request.method));
    });

    test("returns a request with expected handlerPath", () async {
      expect((await handle()).handlerPath, equals(expectedHandlerPathAfter));
    });
  }
}
