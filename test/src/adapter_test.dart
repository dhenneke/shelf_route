library shelf_route.router.adapter.test;

import 'package:test/test.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_route/src/router_builder.dart';
import 'package:shelf_route/src/default_adapters.dart';
import 'helpers.dart';
import 'dart:async';
import 'package:shelf_route/src/default_route_impl.dart';
import 'package:shelf_route/src/default_segment_router.dart';

main() {
  group('DefaultSimpleRouteAdapter', () {
    group('adapt', () {
      SimpleRouteSpec routeSpec;
      DefaultSimpleRouteImpl adapt() => routeSpec.routeAdapter.adapt(routeSpec);
      DefaultSegmentRouter segmentRouter() => adapt().segmentRouter;
      Future<Response> response() => adapt().handle(createRequest('foo'));

      setUp(() {
        final adapter = new DefaultSimpleRouteAdapter.def();

        routeSpec = new SimpleRouteSpec('foo', 'foo', ['GET', 'POST'], false,
            testHandler('hi'), adapter, testMiddleware(1));
      });

      test('returns non null result', () {
        expect(adapt(), isNotNull);
      });

      test('returns non null handler', () {
        expect(adapt().handler, isNotNull);
      });

      test('returns non null segmentRouter', () {
        expect(adapt().segmentRouter, isNotNull);
      });

      test('returns segmentRouter with expected path', () {
        expect(segmentRouter().path.toString(), 'foo');
      });

      test('returns segmentRouter with expected methods', () {
        expect(segmentRouter().methods, unorderedEquals(['GET', 'POST']));
      });

      test('route handles with expected result', () async {
        expect((await response()), new isInstanceOf<Response>());
      });

      test('route handles with expected request middleware crumbs', () async {
        expect(getCrumbs((await response()).context, breadcrumbsIn),
            orderedEquals([1]));
      });
    });

    group('merge', () {
      SimpleRouteSpec routeSpec;
      DefaultRouterAdapter routerAdapter;
      DefaultSimpleRouteAdapter merge() =>
          routeSpec.routeAdapter.merge(routerAdapter);

      DefaultSimpleRouteImpl adapt() => merge().adapt(routeSpec);
      Future<Response> response() async =>
          await (adapt().handle(createRequest('foo')));

      setUp(() {
        routerAdapter = new DefaultRouterAdapter.def();
        final adapter = new DefaultSimpleRouteAdapter.def();

        routeSpec = new SimpleRouteSpec('foo', 'foo', ['GET', 'POST'], false,
            testHandler('hi'), adapter, testMiddleware(1));
      });

      test('isNotNull', () {
        expect(merge(), isNotNull);
      });

      test('route handles with expected result', () async {
        expect((await response()), new isInstanceOf<Response>());
      });

      test('route handles with expected request middleware crumbs', () async {
        expect(getCrumbs((await response()).context, breadcrumbsIn),
            orderedEquals([1]));
      });
    });
  });
}
