library shelf_route.router.defaults.test;

import 'package:test/test.dart';
import 'package:shelf_route/src/route_impl.dart';
import 'package:shelf/shelf.dart';
import 'package:uri/uri.dart';
import 'package:shelf_route/src/core.dart';
import 'package:option/option.dart';
import 'package:shelf_route/src/default_segment_router.dart';

main() {
  DefaultSegmentRouter segmentRouter(
          String path, Iterable<String> methods, bool exactMatch) =>
      new DefaultSegmentRouter(
          new UriParser(new UriTemplate(path)), methods, exactMatch);

  SimpleRouteImpl simple(String path, Iterable<String> methods, bool exactMatch,
          String handlerResult) =>
      new SimpleRouteImpl('fum', segmentRouter(path, methods, exactMatch),
          new TestHandler(handlerResult));

  group('flattened', () {
    test('for simple routes returns just the route', () {
      final route = simple('foo', null, false, 'foo result');
      expect(route.flattened, orderedEquals([route]));
    });

    group('for router with single route', () {
      Iterable<SimpleRouteImpl> flattened;

      setUp(() {
        flattened = new RequestRouterImpl(
            'fum',
            segmentRouter('bar', ['GET', 'OPTION'], true),
            [simple('foo', null, false, 'foo result')],
            const None()).flattened;
      });

      test('has expected number of simple routes', () {
        expect(flattened, hasLength(1));
      });

      test('has expected path', () {
        expect(flattened.first.segmentRouter.path.toString(), 'bar/foo');
      });

      test('has expected methods', () {
        expect(flattened.first.segmentRouter.methods,
            unorderedEquals(['GET', 'OPTION']));
      });
    });

    group('for nested routers', () {
      Iterable<SimpleRouteImpl> flattened;
      Iterable<PathInfo> flattenedPaths;

      setUp(() {
        final router1 = new RequestRouterImpl(
            'fum',
            segmentRouter('bar', ['GET', 'OPTION'], true),
            [simple('foo', null, false, 'foo result')],
            const None());

        final router2 = new RequestRouterImpl(
            'fum',
            segmentRouter('doh', null, false),
            [
              router1,
              simple('blah', ['GET'], true, 'blah result')
            ],
            const None());

        flattened = router2.flattened;
        flattenedPaths = router2.flattenedPaths;
      });

      test('has expected number of simple routes', () {
        expect(flattened, hasLength(2));
      });

      test('has expected paths', () {
        expect(flattened.first.segmentRouter.path.toString(), 'doh/bar/foo');
        expect(flattened.last.segmentRouter.path.toString(), 'doh/blah');
      });

      test('has expected methods', () {
        expect(flattened.first.segmentRouter.methods,
            unorderedEquals(['GET', 'OPTION']));
        expect(flattened.last.segmentRouter.methods, unorderedEquals(['GET']));
      });

      test('has expected flattened paths', () {
        expect(flattenedPaths.map((fp) => fp.path),
            orderedEquals(['/doh/bar/foo', '/doh/blah']));
        expect(flattenedPaths.map((fp) => fp.method),
            orderedEquals(['GET,OPTION', 'GET']));
      });
    });
  });
}

class TestHandler {
  final String result;

  TestHandler(this.result);

  call(Request request) {
    return result;
  }
}
